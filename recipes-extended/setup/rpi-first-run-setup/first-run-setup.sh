#!/bin/sh

# Initially based on the scripts by JohnX/Mer Project - http://wiki.maemo.org/Mer/
# Reworked for the OpenPandora - John Willis/Michael Mrozek
# Quickly 'hacked' for the Raspberry Pi to provide a simple 1st boot wizard.

# Ensure there is a wheel group for sudoers to be put into.
# TODO: Do this somewhere better.
groupadd wheel

# Set the root password
rootusername="root"
rootpassword="raspiroot"
passwd "$rootusername" <<EOF
$rootpassword
$rootpassword
EOF

# Set up the user accounts
fullname1="Operator"
username1="operator"
password1="apollo__123"
fullname2="Engineer"
username2="engineer"
password2="apollo__456"
fullname3="Field Service Engineer"
username3="fseng"
password3="artista"

useradd -c "$fullname1,,," -g users "$username1"

passwd "$username1" <<EOF
$password1
$password1
EOF

useradd -c "$fullname2,,," -G wheel,users "$username2"

passwd "$username2" <<EOF
$password2
$password2
EOF

useradd -c "$fullname3,,," -G wheel,users "$username3"

passwd "$username3" <<EOF
$password3
$password3
EOF

# Add the new user to the sudoers list
echo "$username1 ALL=ALL,!SHELLS,!SU" >> /etc/sudoers
echo "$username2 ALL=(ALL:ALL) ALL" >> /etc/sudoers
echo "$username3 ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# Update the environment PATH
# Note - /sbin and /usr/sbin are only added to the path for the root user.  The following
# export adds those directories for the newly added user
echo "export PATH=$PATH" >> /home/$username3/.bashrc
echo "alias l='ls -CF'" >> /home/$username3/.bashrc
echo "alias la='ls -A'" >> /home/$username3/.bashrc
echo "alias ll='ls -alF'" >> /home/$username3/.bashrc
echo "" >> /home/$username3/.bashrc
echo "sleep 3" >> /home/$username3/.bashrc
echo "sudo chmod ugo+rwx /var/www/htdocs/ifconfig.html" >> /home/$username3/.bashrc
echo "sudo ifconfig eth0 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}' > /var/www/htdocs/ifconfig.html" >> /home/$username3/.bashrc
echo "" >> /home/$username3/.bashrc
echo "case \$(tty) in /dev/tty1)" >> /home/$username3/.bashrc
echo "sudo wpe-init" >> /home/$username3/.bashrc
echo "esac" >> /home/$username3/.bashrc

#echo "export PATH=$PATH:/usr/sbin:/sbin" >> /home/$username/.bashrc
#if [[ "$PATH" != "*sbin*" ]] ; then
#    export PATH="/sbin:/usr/sbin:$PATH" >> /home/$username/.bash_profile
#fi

echo "set ts=4" >> /home/$username3/.vimrc
echo "set sw=4" >> /home/$username3/.vimrc
echo "set autoindent" >> /home/$username3/.vimrc
echo "set expandtab" >> /home/$username3/.vimrc
echo "set showmatch" >> /home/$username3/.vimrc
echo "set showmode" >> /home/$username3/.vimrc
echo "set hlsearch" >> /home/$username3/.vimrc
echo "set tags=./tags,tags;" >> /home/$username3/.vimrc
echo "syntax on" >> /home/$username3/.vimrc
echo "set nobackup" >> /home/$username3/.vimrc
echo "set nowritebackup" >> /home/$username3/.vimrc
echo "set noswapfile" >> /home/$username3/.vimrc

cp /home/$username3/.vimrc /home/root/
echo "<p style='padding: 0px 150px 0px'>" >> /var/www/htdocs/about.html
echo "<br><br>" >> /var/www/htdocs/about.html
uname -srvm >> /var/www/htdocs/about.html
echo "</p>" >> /var/www/htdocs/about.html

# Lock the root account
usermod -L "$rootusername"

# Pick a name for the machine.
mac_addr=$(sed 's/://g' /sys/class/net/eth0/address)
hostname="apollo_$mac_addr"
hostname_file="/etc/hostname"
hosts_file="/etc/hosts"

echo $hostname > $hostname_file
hostname =$(sed 's/ /_/g' $hostname_file)
echo $hostname > $hostname_file
echo "127.0.0.1 localhost.localdomain localhost $hostname" > $hosts_file
hostname -F $hosts_file

control_file="/etc/rpi/first-boot"

# Write the control file so this script is not run on next boot 
# (hackish I know but I want the flexability to drop a new script in later esp. in the early firmwares).

touch $control_file
# Prevent the control file from being erased, which would rerun the initial setup on next boot
chmod 0444 $control_file

# Remove THIS script (especially since it contains sensitive info)
rm -f $0

reboot


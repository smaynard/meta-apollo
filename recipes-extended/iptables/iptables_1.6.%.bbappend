FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
           file://iptables \
           file://iptables.conf \
           "

do_install_append() {
    install -d ${D}${sysconfdir}/network/if-up.d
    install -m 0755 ${WORKDIR}/iptables.conf ${D}${sysconfdir}/iptables.conf
    install -m 0755 ${WORKDIR}/iptables ${D}${sysconfdir}/network/if-up.d/iptables
}

CONFFILES_${PN} += "${sysconfdir}/iptables \
                    ${sysconfdir}/network/if-up.d/iptables.conf \
                    "


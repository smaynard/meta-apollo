DESCRIPTION = "Apollo Packagegroup"
LICENSE = "CLOSED"

inherit packagegroup

PACKAGES = "\
    packagegroup-apollo \
"

RDEPENDS_packagegroup-apollo = "\
                   dnsmasq \
                   iptables \
                   glib-2.0 \
                   fbcp \
                   tslib \
                   tslib-conf \
                   tslib-calibrate \
                   tslib-dev \
                   tslib-tests \
                   wpewebkit \
                   wpebackend \
                   wpebackend-rdk \
                   wpewebkit-web-inspector-plugin \
                   wpelauncher \
                   kiosk-app \
                   libsuinput \
                   tree \
                   evtest \
                   utouch-evemu \
                   libwebsockets \
                   libprotocol-lws-server-status \
                   ntp \
                   iftop \
                   usbutils \
                   rpi-first-run-setup \
"

# Additional OSS packages etc, which are only needed for WPE based images.
RDEPENDS_packagegroup-apollo += "\
    fontconfig \
    fontconfig-utils \
    ttf-bitstream-vera \
"


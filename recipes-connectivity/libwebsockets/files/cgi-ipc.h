
#ifndef __CGI_IPC__
#define __CGI_IPC__

typedef enum {
    ADC_LOOPBACK = 0,
    ID_GAGES,
    MIRROR_INPUTS,
    OUTPUT_DIAGS,
    NO_ADC_RESYNC,
    DISP_REFRESH,
    SLAVE,
    NORMAL
} MODE;

typedef enum {
    GFLAGS_NONE = 0,
    ANALOG_OUT  = 0x01,                // This gage pipe is for analog output
    INVERT_JOG  = 0x02,                // Invert the jog for this gage pipe
    LATCH_RDNG  = 0x04,                // Latch the reading from this gage pipe
    GFLAG_x08   = 0x08,                //  unused
    DIA_MASK    = 0xF0                 // diameter selection for this gage pipe
} GFLAGS;

#define DIAMETER(gf)    (((gf) & DIA_MASK) >> 4)

typedef enum {
    SETUP       = 0x01,                // AUTO = ((SFLAGS & SETUP) == 0);
    FAULT       = 0x02,                // FAULT bit for SETUP or AUTO
    METRIC      = 0x04,                // display metric units (else english)
    MULTI_LANG  = 0x08,                // Enable multi-language support
    MULTI_PART  = 0x10,                // Enable multi-part support
    SFLAG_x20   = 0x20,                //  unused
    SFLAG_x40   = 0x40,                //  unused
    RUNOFF      = 0x80                 // Runoff mode for testing
} SFLAGS;

#define SYS_AUTO(sf)    (((sf) & SETUP) == 0)
#define SYS_SETUP(sf)   ((sf) & SETUP)
#define SYS_FAULT(sf)   ((sf) & FAULT)
#define SYS_METRIC(sf)  ((sf) & METRIC)
#define SYS_RUNOFF(sf)  ((sf) & RUNOFF)

typedef enum {
    EMBED_DATA = 1,                    // IPC packet from the embedded side
    LINUX_DATA,                        // IPC packet from the Linux side
    SETTINGS,                          // IPC from Linux with settings data
    PROFILE,                           // IPC from embedded with profile data
    WDOG,                              // IPC packet with watchdog timestamp
} COMMAND;

typedef enum {
    REPEAT_VAL = 1,                    // Profile TLV with repeating data
    INDIVIDUAL,                        // Profile TLV with individual data
    DIFFERENCE,                        // Profile TLV with difference data
} PROF_TAG;

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

typedef struct {
        u8 seq;                        // sequence counter (0th byte)
        u8 mode;                       // see MODE above
        u8 version;                    // the version of this structure
        u8 cmd;                        // see COMMAND above
        u8 sflags;                     // see system flags (SFLAGS) above
        u8 part;                       // selected part (0 thru 255)
} IPC_header;

#define NGAGES      8
#define DATA_STRUCT_VERSION   1

typedef union __attribute__ ((__packed__, aligned(64))) {
    u8 raw[86];
    struct {
        IPC_header hdr;                // see IPC_header above
        u8 inputs[2];                  // inputs (starts at 8th byte)
        u8 outputs[6];
        u8 gflags[NGAGES];             // gage flags
        u32 readings[NGAGES];          // gage readings (starts at 16th byte)
        u16 dac_out[NGAGES];           // analog/dac out (starts at 48th byte)
        u8 tbd[16];
    } ipc;                             // 86 bytes (+ 42 bytes of header = 128)
} Data;

#endif

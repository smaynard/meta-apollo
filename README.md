# **meta-apollo**

`meta-apollo` is a meta layer for OpenEmbedded / Yocto environments. It provides the necessary recipes to build the kiosk components including the webengine and browser.

# *Setup*

Note: You only need this if you do not have an existing OpenEmbedded Yocto environment.

### Install the following packages on your host linux computer (I use Ubuntu) ###
```
sudo apt-get install build-essential gettext bison libfile-slurp-perl gawk libncurses-dev autoconf flex doxygen libtool automake  libpcre3-dev zlib1g-dev libbz2-dev subversion minicom putty libssl-dev rpm python-pexpect python-svn python-argparse vim tofrodos meld dos2unix cmake uuid-dev ruby transfig libglib2.0-dev xutils-dev lynx-cur gperf autopoint  python-dulwich  python-dev curl vim diffstat texinfo chrpath
```

### Configure bash as default command interpreter for shell scripts ###
```
sudo dpkg-reconfigure dash
```
choose bash, when the prompt asks if you want to use dash as the default system shell - select �No�

### Configure Git ###
```
sudo apt-get install git
```
Once git is installed, configure your name and email using the below commands
```
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```

### Get the google repo tool ###
```
mkdir ~/bin
PATH=~/bin:$PATH
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
```

### Set up .netrc ###
This step prevents the password prompt every now and then during code checkout and build.
Create a file .netrc in home directory
```
vi .netrc
```
The contents of the .netrc should be as follows
```
machine bitbucket.org
login <YOUR_USERNAME>
password <YOUR_PASSWORD>
```

### Create a workspace for your yocto build ###
```
mkdir -p apollo-yocto
```

### Inside the newly created directory, initiate the repository ###
```
cd apollo-yocto
repo init -u https://bitbucket.org/smaynard/meta-apollo.git -b master -m tools/manifests/apollo-yocto.xml
```

### Fetch and sync all repositories ###
```
repo sync
```

# *Build*

### Initialize OE by invoking the init script ###
```
source poky/oe-init-build-env artista
```

### Add bb layers ###

```
bitbake-layers add-layer ../meta-openembedded/meta-oe/

bitbake-layers add-layer ../meta-openembedded/meta-multimedia/

bitbake-layers add-layer ../meta-openembedded/meta-python/

bitbake-layers add-layer ../meta-openembedded/meta-networking/

bitbake-layers add-layer ../meta-wpe

bitbake-layers add-layer ../meta-raspberrypi

bitbake-layers add-layer ../meta-apollo

```


### Edit `conf/local.conf` and set the target machine ###
```
MACHINE = "raspberrypi3"

DISTRO = "apollo"

DL_DIR ?= "$HOME/bitbake-downloads"

RM_OLD_IMAGE = "1"

PACKAGECONFIG_append_pn-cairo = " glesv2"
PACKAGECONFIG_append_pn-wpewebkit = " 2dcanvas"

WPE_BACKEND_rpi = "rpi"
DISTRO_FEATURES_remove = "x11 wayland"
DISTRO_FEATURES_append = " opengl"
BBMASK += "westeros meta-wpe/recipes-kernel/linux meta-raspberrypi/recipes-core/psplash"
SERIAL_CONSOLE = "115200 ttyAMA0"



```

### Build the Apollo Linux distribution ###
```
bitbake apollo-image
```

# *Install*

To flash the sdimg on the sd card:
```
sudo diskutil unmountDisk /dev/disk4
sudo dd if=tmp/deploy/images/raspberrypi3/artista-raspberrypi3.rpi-sdimg of=/dev/rdisk4 bs=1m
sudo diskutil unmountDisk /dev/disk4
```


# *Optional settings*

Use 4.9 kernel (optional), edit `conf/local.conf` and set
```
PREFERRED_VERSION_linux-raspberrypi = "4.9%"

DL_DIR ?= "/home/<USERNAME>/bitbake-downloads"

EXTRA_IMAGE_FEATURES ?= "debug-tweaks tools-sdk tools-debug tools-profile"


```

To build an initramfs image, edit `conf/local.conf` and set
```
INITRAMFS_IMAGE_BUNDLE_rpi = "1"
INITRAMFS_IMAGE_rpi = "apollo-image"
KERNEL_INITRAMFS = "-initramfs"
BOOT_SPACE = "163840"
```

To install the initram fs:
```
copy the zImage to the fat32 partition of your rpi3
```

To change the boot image:
```
use gimp and modify logo_apollo.xcf the way you want it, then:
ppmquant 224 logo_apollo.ppm > logo_apollo_clut.ppm
pnmnoraw logo_apollo_clut.ppm > logo_apollo_clut224.ppm 
cp logo_apollo_clut224.ppm ~/apollo-yocto/artista/tmp/work-shared/raspberrypi3/kernel-source/drivers/video/logo/logo_apollo_clut224.ppm 
vi 0001-Adding-apollo-boot-logo.patch and delete the last sectioon (logo)
diff -Naur /dev/null tmp/work-shared/raspberrypi3/kernel-source/drivers/video/logo/logo_apollo_clut224.ppm >> ~/apollo-yocto/meta-apollo/recipes-kernel/linux/files/0001-Adding-apollo-boot-logo.patch 

```


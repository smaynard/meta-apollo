SUMMARY = "Example U/I"
BUGTRACKER = ""

LICENSE = "CLOSED"
SECTION = "net"
SRC_URI = "file://index.html \
           file://wpe-init \
           file://autologin \
           file://css/style.css \
           file://js/jquery-1.6.4.min.js \
          "

S = "${WORKDIR}"

TARGET_CC_ARCH += "${LDFLAGS}" 


do_install() {
    echo "<table style='width:100%'><tbody><tr><td></td><td><h3>Apollo-Yocto Project</h3></td><td></td><td><h3>Modules</h3></td></tr>" > ${S}/about.html

    echo "<tr><th align='right'>User Interface:</th><td>1.00.$(cd ~/apollo-yocto/meta-apollo;git log -1 --pretty=format:%h)</td><th align='right'></td></tr>" >> ${S}/about.html

    echo "<tr></tr><tr><td></td><td><h3>OS</h3></td></tr>" >> ${S}/about.html

    echo "<tr><th align='right'>meta-bsp:</th><td>1.00.$(cd ~/apollo-yocto/poky;git log -1 --pretty=format:%h)</td></tr>" >> ${S}/about.html
    echo "<tr><th align='right'>meta-pi:</th><td>1.00.$(cd ~/apollo-yocto/meta-raspberrypi;git log -1 --pretty=format:%h)</td></tr>" >> ${S}/about.html
    echo "<tr><th align='right'>meta-oe:</th><td>1.00.$(cd ~/apollo-yocto/meta-openembedded;git log -1 --pretty=format:%h)</td></tr>" >> ${S}/about.html
    echo "<tr><th align='right'>meta-wpe:</th><td>1.00.$(cd ~/apollo-yocto/meta-wpe;git log -1 --pretty=format:%h)</td></tr>" >> ${S}/about.html
    echo "<tr><th align='right'>meta-apollo:</th><td>1.00.$(cd ~/apollo-yocto/meta-apollo;git log -1 --pretty=format:%h)</td></tr></tbody></table>" >> ${S}/about.html

    install -d ${D}${localstatedir}/www/htdocs/css
    install -d ${D}${localstatedir}/www/htdocs/js
    install -d ${D}${bindir}

    install -m 0777 /dev/null ${D}${localstatedir}/www/htdocs/ifconfig.html
    install -m 0755 ${S}/about.html ${D}${localstatedir}/www/htdocs/
    install -m 0755 ${WORKDIR}/index.html ${D}${localstatedir}/www/htdocs/
    install -m 0755 ${WORKDIR}/wpe-init ${D}${bindir}
    install -m 0755 ${WORKDIR}/autologin ${D}${bindir}
    install -m 0755 ${WORKDIR}/css/style.css ${D}${localstatedir}/www/htdocs/css/
    install -m 0755 ${WORKDIR}/js/jquery-1.6.4.min.js ${D}${localstatedir}/www/htdocs/js/
}

PACKAGE_ARCH = "${MACHINE_ARCH}"

FILES_${PN} += "${localstatedir}/www/htdocs/"


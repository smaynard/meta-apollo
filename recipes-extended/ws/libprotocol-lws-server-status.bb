#
# protocol-lws-server-status recipe
#

require ./websockets.inc

PACKAGES =+ "libprotocol_lws_server_status"

do_install_append() {
    install -m 0755 ${WORKDIR}/build/status/.libs/libprotocol_lws_server_status.so.0.0.0 ${D}${datadir}/libwebsockets-test-server/plugins/libprotocol_lws_server_status.so
}

FILES_${PN} += "${datadir}/libwebsockets-test-server/plugins/libprotocol_lws_server_status.so"


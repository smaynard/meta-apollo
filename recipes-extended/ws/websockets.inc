#
# websockets recipe
#

SUMMARY = "WebSockets for the Kiosk App"
BUGTRACKER = ""
SECTION = "extended"
DEPENDS = "libwebsockets glib-2.0"
LICENSE = "CLOSED"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRCREV = "master"
SRC_URI = "git://git@bitbucket.org/smaynard/apollo-websockets.git;protocol=ssh"
inherit autotools pkgconfig

S = "${WORKDIR}/git"

INHIBIT_PACKAGE_DEBUG_SPLIT = '1'
INHIBIT_PACKAGE_STRIP = '1'
SOLIBS = ".so"
FILES_SOLIBSDEV = ""

do_configure() {
    NOCONFIGURE=true ${S}/autogen.sh
    oe_runconf
}

do_install() {
    install -d ${D}${datadir}/libwebsockets-test-server/plugins/
    install -d ${D}${localstatedir}/www/htdocs/
}


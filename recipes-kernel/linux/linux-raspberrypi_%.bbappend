FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://0001-Adding-apollo-boot-logo.patch \
    file://logo_apollo_clut224.ppm \
"

PRIORITY = "120"

do_configure_prepend() {
    kernel_configure_variable LOGO_LINUX_CLUT224 n
    kernel_configure_variable LOGO_APOLLO_CLUT224 y

    cp ${WORKDIR}/logo_apollo_clut224.ppm ${B}/source/drivers/video/logo/logo_apollo_clut224.ppm
}


include recipes-core/images/core-image-minimal.bb

DESCRIPTION = "Apollo Display Technologies Linux distro for RaspberryPi3"
LICENSE = "CLOSED"

inherit distro_features_check image-buildinfo buildhistory

REQUIRED_DISTRO_FEATURES = ""

SPLASH_rpi = "psplash-default"


IMAGE_FEATURES += " hwcodecs \
                    package-management \
                    ssh-server-openssh \
                    splash \
"

IMAGE_INSTALL += " gawk \
                   packagegroup-apollo \
                   kernel-modules \
                   git \
                   vim \
"

export IMAGE_BASENAME = "apollo"


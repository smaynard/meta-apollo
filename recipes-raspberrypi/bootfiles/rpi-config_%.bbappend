SUMMARY = "raspi boot mods"
BUGTRACKER = ""

LICENSE = "CLOSED"
SECTION = "boot"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " file://dt-blob.bin \
          "
do_deploy_append() {
	# display settings
	echo "hdmi_group=2" >>${DEPLOYDIR}/bcm2835-bootfiles/config.txt
	echo "hdmi_mode=28" >>${DEPLOYDIR}/bcm2835-bootfiles/config.txt
	echo 'dtoverlay=rpi-ft5406' >> ${DEPLOYDIR}/bcm2835-bootfiles/config.txt
	echo 'enable_uart=1' >> ${DEPLOYDIR}/bcm2835-bootfiles/config.txt
	echo 'dtparam=spi=on' >> ${DEPLOYDIR}/bcm2835-bootfiles/config.txt
	echo 'dtparam=i2c_arm=on' >> ${DEPLOYDIR}/bcm2835-bootfiles/config.txt
	echo "dwc_otg.lpm_enable=0 console=serial0,115200 root=/dev/mmcblk0p2 rootfstype=ext4 rootwait" >${DEPLOYDIR}/bcm2835-bootfiles/cmdline.txt
    cp ${WORKDIR}/dt-blob.bin ${DEPLOYDIR}/bcm2835-bootfiles/
}


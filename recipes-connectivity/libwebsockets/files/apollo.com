
{
    "vhosts": [{
        "name": "apollo.com",
        "port": "80",
        "noipv6": "on",
        "mounts": [{  # autoserve
            "mountpoint": "/",
            "origin": "file:///var/www/htdocs/",
            "default": "index.html"
        }],
        "ws-protocols": [{
            "lws-server-status": {
            "status": "ok",
            "update-ms": "3000"
            }
        }]
    }]
}


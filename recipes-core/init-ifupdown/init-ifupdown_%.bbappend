EXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "\
    file://interfaces \
"

do_install_append(){
    install -d ${D}${sysconfdir}
    install -d ${D}${sysconfdir}/network/
    install -m 644 ${WORKDIR}/interfaces ${D}${sysconfdir}/network/
}

FILES_${PN} += "${sysconfdir}/network/interfaces"

CONFFILES_${PN} += "${sysconfdir}/network/interfaces"


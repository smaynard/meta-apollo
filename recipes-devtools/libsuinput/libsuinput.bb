#
# libsuinput recipe
#

SUMMARY = "libsuinput for test harness"
BUGTRACKER = ""
SECTION = "devtools"
DEPENDS = "udev"
LICENSE = "CLOSED"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRCREV = "master"
SRC_URI = "git://github.com/tuomasjjrasanen/libsuinput;protocol=http"

inherit autotools pkgconfig

S = "${WORKDIR}/git"

do_compile() {
    make
}


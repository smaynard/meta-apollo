
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
           file://conf \
           file://cgi-ipc.h \
           file://apollo.com \
           file://lwsws.service \
           file://lwsws.init \
           file://server-status.html \
           file://lwsws-logo.png \
           file://lws-common.js \
           "

inherit update-rc.d systemd

PACKAGECONFIG = "libuv client server plugins status shared rpath"
PACKAGECONFIG[shared] = "-DLWS_WITH_SHARED=ON,-DLWS_WITH_SHARED=OFF,"
PACKAGECONFIG[server] = "-DLWS_WITH_LWSWS=ON,-DLWS_WITH_LWSWS=OFF,"
PACKAGECONFIG[plugins] = "-DLWS_WITH_PLUGINS=ON,-DLWS_WITH_PLUGINS=OFF,"
PACKAGECONFIG[status] = "-DLWS_WITH_SERVER_STATUS=ON,-DLWS_WITH_SERVER_STATUS=OFF,"
PACKAGECONFIG[rpath] = "-DCMAKE_SKIP_RPATH=ON,-DCMAKE_SKIP_RPATH=OFF,"

# for sysvinit
INITSCRIPT_NAME = "lwsws"
INITSCRIPT_PARAMS = "defaults 70"

# for systemd
SYSTEMD_SERVICE_${PN} = "lwsws.service"

do_install_append() {
    install -d ${D}${includedir}
    install -m 0755 ${WORKDIR}/cgi-ipc.h ${D}${includedir}/
    install -d ${D}${sysconfdir}/lwsws/conf.d
    install -m 0755 ${WORKDIR}/conf ${D}${sysconfdir}/lwsws/
    install -m 0755 ${WORKDIR}/apollo.com ${D}${sysconfdir}/lwsws/conf.d/
    install -d ${D}${localstatedir}/www/htdocs/
    install -m 0755 ${WORKDIR}/server-status.html ${D}${localstatedir}/www/htdocs/
    install -m 0755 ${WORKDIR}/lwsws-logo.png ${D}${localstatedir}/www/htdocs/
    install -m 0755 ${WORKDIR}/lws-common.js ${D}${localstatedir}/www/htdocs/

    if ${@bb.utils.contains('DISTRO_FEATURES','sysvinit','true','false',d)}; then
        install -d ${D}${includedir}
        install -m 0755 ${WORKDIR}/build/include/${PN}.h ${D}${includedir}/
        install -d ${D}${sysconfdir}/init.d
        install -m 0755 ${WORKDIR}/lwsws.init ${D}${sysconfdir}/init.d/lwsws
    elif ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then
        install -d ${D}${systemd_system_unitdir}
        install -m 644 ${WORKDIR}/lwsws.service ${D}/${systemd_system_unitdir}
    fi
}

FILES_${PN} += "${bindir}/lwsws \
                ${systemd_system_unitdir}/lwsws.service \
                ${localstatedir}/www/htdocs/* \
                ${datadir}/${PN}*"

FILES_${PN}-dev += "${includedir}/${PN}.h"

CONFFILES_${PN} += "${sysconfdir}/lwsws/*"

